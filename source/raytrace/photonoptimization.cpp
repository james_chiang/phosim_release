///
/// @package phosim
/// @file photonoptimization.cpp
/// @brief photon optimization routines (part of Image class)
///
/// @brief Created by:
/// @author John R. Peterson (Purdue)
///
/// @brief Modified by:
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///

int Image::dynamicTransmissionOptimization(long k, long *lastSurface, long *preGhost, long waveSurfaceIndex, long straylightcurrent) {

    long newSurf;

 redoghlp:;
    newSurf = -1;
    photon.direction = 1;
    photon.counter = -1;
    *preGhost = 0;

    for (int i = -1; i < natmospherefile; i++) {

        if (i >= 0) {
            if (photon.counter >= (MAX_BOUNCE - 1)) goto maxbounce;
            if (transmissionPreCheck(i*2 + 1, waveSurfaceIndex)) {
                if (k > 0) goto redoghlp;
                return(1);
            }
        }

        if (photon.counter >= (MAX_BOUNCE - 1)) goto maxbounce;
        if (transmissionPreCheck(i*2 + 2, waveSurfaceIndex)) {
            if (k > 0) goto redoghlp;
            return(1);
        }

    }
    while (1) {
        if (photon.direction == 1) {
            newSurf++;
        } else {
            newSurf--;
        }
        if (newSurf == -1) {
            if (k > 0) goto redoghlp;
            return(1);
        }

        if (throughputfile == 1 && photon.direction == 1) {
            *lastSurface = newSurf;
        }
        if (photon.counter >= (MAX_BOUNCE - 1)) goto maxbounce;
        if (transmissionPreCheck(natmospherefile*2 + 1 + 2*newSurf, waveSurfaceIndex)) {
            if (straylightcurrent == 1) {
                if (photon.counter >= (MAX_BOUNCE - 1)) goto maxbounce;
                if (transmissionPreCheck(natmospherefile*2 + 1 + 2*newSurf + 1, waveSurfaceIndex)) {
                    if (k > 0) goto redoghlp;
                    return(1);
                } else {
                    photon.direction = -photon.direction;
                    (*preGhost)++;
                }
            } else {
                if (k > 0) goto redoghlp;
                return(1);
            }
        }
        if (photon.direction == 1 && newSurf == nsurf - 1) {
            if (photon.counter >= (MAX_BOUNCE - 1)) goto maxbounce;
            if (transmissionPreCheck(natmospherefile*2 + 1 + 2*nsurf, waveSurfaceIndex)) {
                if (k > 0) goto redoghlp;
                return(1);
            }
            goto maxbounce;
        }

        if (photon.direction == -1 && newSurf == 0) {
            if (k > 0) goto redoghlp;
            return(1);
        }
    }
 maxbounce:;
    photon.maxcounter = photon.counter;

    return(0);

}
