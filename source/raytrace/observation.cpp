///
/// @package phosim
/// @file observation.cpp
/// @brief observation class for raytrace code
///
/// @brief Created by:
/// @author John R. Peterson (Purdue)
///
/// @brief Modified by:
/// @author Alan Meert (Purdue)
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///

#include <iostream>
#include <sstream>
#include <fstream>
#include <zlib.h>
#include <string.h>
#include <math.h>
#include <fitsio.h>
#include <fitsio2.h>

#include "raytrace.h"
#include "observation.h"
#include "constants.h"
#include "helpers.h"
#include "constants.h"
#include "ancillary/readtext.h"

using readtext::readText;

#include "settings.cpp"

int Observation::addSource (const std::string & object, int sourcetype) {

    char tempstring[4096];
    int nspatialpar, ndustpar, ndustparz;
    long i, j;
    char tempstring1[512];
    char tempstring2[512];
    double tempf1, tempf2;
    long lsedptr;
    FILE *indafile;
    gzFile ingzfile;
    char line[4096];
    int status;
    char *ffptr;
    fitsfile *faptr;
    long naxes[2];
    int nfound;
    int anynull;
    float nullval;
    double cdw, dw;
    long closestw = 0;
    double x, y;
    double nn;
    double mag;
    long badfile, oldsed;
    char *sptr, *sptr2, *sptr3;
    double ra, dec, id, redshift, gamma1, gamma2, kappa, magnification,
        deltara, deltadec;

    nspatialpar = 0;

    std::istringstream iss(object);
    iss >> id >> ra >> dec;
    sources.id.push_back(id);
    sources.ra.push_back(ra);
    sources.dec.push_back(dec);
    sources.ra[nsource] *= M_PI/180.0;
    sources.dec[nsource] *= M_PI/180.0;
    iss >> mag >> sources.sedfilename[nsource] >> redshift >> gamma1 >> gamma2 >> kappa >> deltara >> deltadec;
    sources.redshift.push_back(redshift);
    sources.gamma1.push_back(gamma1);
    sources.gamma2.push_back(gamma2);
    sources.kappa.push_back(kappa);
    sources.deltara.push_back(deltara);
    sources.deltadec.push_back(deltadec);

    sources.type[nsource] = sourcetype;
    magnification = 2.5*log10((1 - sources.kappa[nsource])*(1 - sources.kappa[nsource])-
                              sources.gamma1[nsource]*sources.gamma1[nsource]-
                              sources.gamma2[nsource]*sources.gamma2[nsource]);
    sources.norm[nsource] = pow(10.0, ((mag + magnification + 48.6)/(-2.5)));
    sources.mag[nsource] = mag + magnification;

    oldsed = 0;
    // read SED file
    if (nsource > 0) {
        for (i = 0; i < nsource; i++) {
            if (sources.sedfilename[i] == sources.sedfilename[nsource]) {
                sources.sedptr[nsource] = sources.sedptr[i];
                oldsed = 1;
                goto skipsedread;
            }
        }
    }

    sources.sedptr[nsource] = nsedptr;

    if (sedptr == 0) {
        nreallocs = 0;
        sed_max = 4194304; // 4M elements, 32MB memory
        sed_w = static_cast<double*>(malloc((long)(sed_max*sizeof(double))));
        sed_c = static_cast<double*>(malloc((long)(sed_max*sizeof(double))));
    } else {
        if (sedptr > (sed_max - 25000)) {
            ++nreallocs;
            sed_max = 2*sed_max;
            sed_w = static_cast<double*>(realloc(sed_w, (long)((sed_max)*sizeof(double))));
            sed_c = static_cast<double*>(realloc(sed_c, (long)((sed_max)*sizeof(double))));
        }
    }

    lsedptr = 0;
    sprintf(tempstring, "%s/%s", seddir.c_str(), sources.sedfilename[nsource].c_str());
    if (flatdir == 1) {
        sptr = strtok_r(tempstring, "/", &sptr2);
        do {
            sptr3 = sptr;
            sptr = strtok_r(NULL, "/", &sptr2);
        } while (sptr != NULL);
        sprintf(tempstring, "%s", sptr3);
    }

    if (strstr(tempstring, ".gz") == NULL) {

        indafile = fopen(tempstring, "r");
        if (indafile == NULL) {
            fprintf(stderr, "Can't find SED file: %s\n", tempstring);
            exit(1);
        }

        closestw = 0;
        cdw = 1e30;
        badfile = 0;
        while (fgets(line, 4096, indafile)) {
            sscanf(line, "%s %s", tempstring1, tempstring2);
            tempf1 = strtod(tempstring1, NULL);
            tempf2 = strtod(tempstring2, NULL);
            *(sed_w + sedptr + lsedptr) = tempf1;
            *(sed_c + sedptr + lsedptr) = tempf2*tempf1*1e-7/(1.98645e-16);
            if (tempstring2[0] == 'n' || tempstring2[0] == 'N') {
                if (badfile == 0) {
                    fprintf(stderr, "Error:   SED file: %s contains a NaN!\n", tempstring);
                }
                *(sed_c + sedptr + lsedptr) = 1e-20;
                badfile = 1;
            }

            dw = fabs(tempf1 - 500.0);

            if (dw < cdw && tempf2 > 0.0) {
                cdw = dw;
                closestw = lsedptr;
            }
            lsedptr = lsedptr + 1;
            if (lsedptr >= 25000) {
                fprintf(stderr, "Error:  Too many lines in SED file: %s\n", tempstring);
                exit(1);
            }
        }
        fclose(indafile);

    } else {

        ingzfile = gzopen(tempstring, "r");
        if (ingzfile == NULL) {
            fprintf(stderr, "Can't find SED file: %s\n", tempstring);
            exit(1);
        }

        closestw = 0;
        cdw = 1e30;
        badfile = 0;
        while (gzgets(ingzfile, line, 4096)) {
            sscanf(line, "%s %s", tempstring1, tempstring2);
            tempf1 = strtod(tempstring1, NULL);
            tempf2 = strtod(tempstring2, NULL);

            *(sed_w + sedptr + lsedptr) = tempf1;
            *(sed_c + sedptr + lsedptr) = tempf2*tempf1*1e-7/(1.98645e-16);
            if (tempstring2[0] == 'n' || tempstring2[0] == 'N') {
                if (badfile == 0) fprintf(stderr, "Error:   SED file: %s contains a NaN!\n", tempstring);
                *(sed_c + sedptr + lsedptr) = 1e-20;
                badfile = 1;
            }

            dw = fabs(tempf1 - 500.0);

            if (dw < cdw && tempf2 > 0.0) {
                cdw = dw;
                closestw = lsedptr;
            }
            lsedptr = lsedptr + 1;
            if (lsedptr >= 25000) {
                fprintf(stderr, "Error:  Too many lines in SED file: %s\n", tempstring);
                exit(1);
            }
        }
        gzclose(ingzfile);

    }


    for (j = 0; j < lsedptr; j++) {
        if (j != 0 && j != (lsedptr - 1)) *(sed_c + sedptr + j) = *(sed_c + sedptr + j)*(*(sed_w + sedptr + j + 1) - *(sed_w + sedptr + j - 1))/2.0;
        if (j == 0) *(sed_c + sedptr + j) = *(sed_c + sedptr + j)*(*(sed_w + sedptr + j + 1) - *(sed_w + sedptr + j));
        if (j == (lsedptr - 1)) *(sed_c + sedptr + j) = *(sed_c + sedptr + j)*(*(sed_w + sedptr + j) - *(sed_w + sedptr + j - 1));
    }

    tempf1 = 0;
    for (j = 0; j < lsedptr; j++) {
        tempf1 += *(sed_c + sedptr + j);
    }
    for (j = 0; j < lsedptr; j++) {
        *(sed_c + sedptr + j)=*(sed_c + sedptr + j)/tempf1;
    }

    if (closestw == 0) {
        sed_dwdp[nsedptr] = (*(sed_w + sedptr + closestw + 1) - *(sed_w + sedptr + closestw))/(*(sed_c + sedptr + closestw))/1.0;
    } else {
        sed_dwdp[nsedptr] = (*(sed_w + sedptr + closestw + 1) - *(sed_w + sedptr + closestw - 1))/(*(sed_c + sedptr + closestw))/2.0;
    }
    if (*(sed_c + sedptr + closestw) <= 0.0) {
        printf("Error in SED file; 0 value at 500 nm\n");
        sed_dwdp[nsedptr] = 0.0;
    }

    for (j = 1; j < lsedptr; j++) {
        *(sed_c + sedptr + j) += *(sed_c + sedptr + j - 1);
    }

    if (oldsed == 0) {
        sed_n[nsedptr] = lsedptr;
        sed_ptr[nsedptr] = sedptr;
        sedptr = sedptr + lsedptr;
        nsedptr++;
    }
    if (nsedptr >= 10000) {
        printf("Error:   Too many SED files\n");
        exit(1);
    }

 skipsedread:;
    sources.norm[nsource] = sources.norm[nsource]/(500.0)*(1 + sources.redshift[nsource])*sed_dwdp[sources.sedptr[nsource]];

    iss >> sources.spatialname[nsource];

    if (sources.spatialname[nsource] == "point") {
        sources.spatialtype[nsource] = POINT;
        nspatialpar = 0;
    }
    if (sources.spatialname[nsource].find("fit") != std::string::npos) {
        sources.spatialtype[nsource] = IMAGE;
        nspatialpar = 2;
    }
    if (sources.spatialname[nsource] == "gauss") {
        sources.spatialtype[nsource] = GAUSSIAN;
        nspatialpar = 1;
    }
    if (sources.spatialname[nsource] == "sersic") {
        sources.spatialtype[nsource] = SERSIC;
        nspatialpar = 6;
    }
    if (sources.spatialname[nsource] == "sersic2d") {
        sources.spatialtype[nsource] = SERSIC2D;
        nspatialpar = 4;
    }
    if (sources.spatialname[nsource] == "sersic2D") {
        sources.spatialtype[nsource] = SERSIC2D;
        nspatialpar = 4;
    }
    if (sources.spatialname[nsource] == "movingpoint") {
        sources.spatialtype[nsource] = MOVINGPOINT;
        nspatialpar = 2;
    }
    if (sources.spatialname[nsource] == "pinhole") {
        sources.spatialtype[nsource] = PINHOLE;
        nspatialpar = 4;
    }

    if (sources.spatialtype[nsource] == 1) {
        if (nsource > 0) {
            sources.skysameas[nsource] = -1;
            for (i = 0; i < nsource; i++) {
                if (sources.spatialname[nsource] == sources.spatialname[i]) sources.skysameas[nsource] = i;
            }
        } else {
            sources.skysameas[nsource] = -1;
        }


        // read image file

        if (sources.skysameas[nsource] == -1) {
            sprintf(tempstring, "%s/%s+0", imagedir.c_str(), sources.spatialname[nsource].c_str());

            ffptr = tempstring;
            status = 0;

            if (fits_open_file(&faptr, ffptr, READONLY, &status)) printf("Error opening %s\n", ffptr);
            fits_read_keys_lng(faptr, (char*)"NAXIS", 1, 2, naxes, &nfound, &status);
            tempptr[nimage] = static_cast<float*>(malloc(naxes[0]*naxes[1]*sizeof(float)));
            naxesb[nimage][0] = naxes[1];
            naxesb[nimage][1] = naxes[0];
            fits_read_img(faptr, TFLOAT, 1, naxes[0]*naxes[1], &nullval,
                          tempptr[nimage], &anynull, &status);
            fits_close_file(faptr, &status);


            cumulative[nimage] = 0;
            cumulativex[nimage] = (float*)malloc(naxesb[nimage][0]*sizeof(float));
            for (i = 0; i < naxesb[nimage][0]; i++) {
                cumulativex[nimage][i] = cumulative[nimage];
                for (j = 0; j < naxesb[nimage][1]; j++) {
                    if (*(tempptr[nimage] + i*naxesb[nimage][1] + j) < 0) {
                        *(tempptr[nimage] + i*naxesb[nimage][1] + j) = 0;
                    }
                    cumulative[nimage] += *(tempptr[nimage] + i*naxesb[nimage][1] + j);
                }
            }

            sources.spatialpar[nsource][2] = nimage;
            nimage++;
        }

    }



    if (nspatialpar > 0) {
        for (i = 0; i < nspatialpar; i++) {
            iss >> sources.spatialpar[nsource][i];
        }
    }

    iss >> sources.dustnamez[nsource];

    sources.dusttypez[nsource] = 0;
    ndustparz = 0;
    if (sources.dustnamez[nsource] == "ccm") {
        sources.dusttypez[nsource] = 1;
        ndustparz = 2;
    } else if (sources.dustnamez[nsource] == "calzetti") {
        sources.dusttypez[nsource] = 2;
        ndustparz = 2;
    } else if (sources.dustnamez[nsource] == "CCM") {
        sources.dusttypez[nsource] = 1;
        ndustparz = 2;
    } else if (sources.dustnamez[nsource] == "CALZETTI") {
        sources.dusttypez[nsource] = 2;
        ndustparz = 2;
    }

    if (ndustparz > 0) {
        for (i = 0; i < ndustparz; i++) {
            iss >> sources.dustparz[nsource][i];
        }
    }


    iss >> sources.dustname[nsource];

    sources.dusttype[nsource] = 0; ndustpar = 0;
    if (sources.dustname[nsource] == "ccm") {sources.dusttype[nsource] = 1; ndustpar = 2;}
    else if (sources.dustname[nsource] == "calzetti") {sources.dusttype[nsource] = 2; ndustpar = 2;}
    else if (sources.dustname[nsource] == "CCM") {sources.dusttype[nsource] = 1; ndustpar = 2;}
    else if (sources.dustname[nsource] == "CALZETTI") {sources.dusttype[nsource] = 2; ndustpar = 2;}

    if (ndustpar > 0) {
        for (i = 0; i < ndustpar; i++) {
            iss >> sources.dustpar[nsource][i];
        }
    }

    setup_tangent(pra, pdec, &tpx, &tpy, &tpz);

    tangent(sources.ra[nsource] + sources.deltara[nsource], sources.dec[nsource] + sources.deltadec[nsource], &x, &y, &tpx, &tpy, &tpz);

    sources.vx[nsource] = x*cos(rotationangle) - y*sin(rotationangle);
    sources.vy[nsource] = x*sin(rotationangle) + y*cos(rotationangle);
    sources.vz[nsource] = -1.0;
    nn = sqrt((sources.vx[nsource])*(sources.vx[nsource]) +
              (sources.vy[nsource])*(sources.vy[nsource]) + 1);
    sources.vx[nsource] = sources.vx[nsource]/nn;
    sources.vy[nsource] = sources.vy[nsource]/nn;
    sources.vz[nsource] = sources.vz[nsource]/nn;

    nsource++;
    return(0);
}


int Observation::background () {

    char tempstring[4096];
    double focal_length = platescale*180.0/M_PI;
    double A, F, xp, yp, ra, dec, xv, yv, y_max_distance, currbuffer, source_dist_x, source_dist_y, x_max_distance;
    double dx , dy;
    long ndeci,  nrai, deci, rai;
    long over, i, j;
    double dra, dis, cosdis;

    int nspatialpar, ii, jj;
    char tempstring1[512];
    char tempstring2[512];
    double tempf1,  tempf2;
    long lsedptr;
    FILE *indafile;
    gzFile ingzfile;
    char line[4096];
    std::string dir;
    double cdw, dw;
    long closestw = 0;
    double x, y;
    double nn;
    double mag = 100;
    long badfile, oldsed;
    char *sptr, *sptr2, *sptr3;

    int diffusetype;
    double angular_sep_degrees, angular_sep_radians, moon_magnitude, moon_apparent_magnitude,
        scatter_function, moon_illuminance, lunar_illuminance, darksky_magnitude, zodiacallight_magnitude;

    if (flatdir == 0) dir=datadir + "/sky";
    else if (flatdir == 1) dir = ".";

    airglow = (float*)calloc((airglowScreenSize)*(airglowScreenSize), sizeof(float));
    sprintf(tempstring, "airglowscreen_%ld.fits.gz", obshistid);
    {
        char *ffptr;
        fitsfile *faptr;
        long naxes[2];
        int nfound;
        int anynull;
        float nullval;
        int status;

        ffptr = tempstring;
        status = 0;
        if (fits_open_file(&faptr, ffptr, READONLY, &status)) {printf("Error opening %s\n", ffptr); exit(1);}
        fits_read_keys_lng(faptr, (char*)"NAXIS", 1, 2, naxes, &nfound, &status);
        fits_read_img(faptr, TFLOAT, 1, naxes[0]*naxes[1], &nullval, airglow, &anynull, &status);
        fits_close_file(faptr, &status);
    }

    //CALCULATIONS FOR BACKGROUND
    moon_apparent_magnitude = -12.73 + .026 * (fabs(phaseang * 180 / M_PI)) + (4E-9) * pow(phaseang * 180 / M_PI,  4);
    moon_illuminance = pow(10, -0.4 * (moon_apparent_magnitude + 16.57));
    lunar_illuminance = 26.3311157 - 1.08572918 * log(moon_illuminance);

    //TWILIGHT
    float temp;
    float background_magnitude = zenith_v;
    float background_brightness = 34.08 * exp( 20.72333 - .92104 * background_magnitude );
    float darksky[6];
    float darksky_data[2511][2];
    char darksky_sedfile[4096];
    FILE *darksky_fp;

    sprintf( darksky_sedfile, "%s/darksky_sed.txt",dir.c_str());

    darksky_fp = fopen( darksky_sedfile, "r" );
    for ( i = 0; i < 2511; i++ ){
        fgets( line, 4096, darksky_fp );
        sscanf( line, "%f %f\n", &darksky_data[i][0], &temp );
        darksky_data[i][1] = temp * background_brightness / 6.299537E-18;
    }
    fclose( darksky_fp );
    darksky[0] = darksky_data[39][1];
    darksky[1] = darksky_data[491][1];
    darksky[2] = darksky_data[1019][1];
    darksky[3] = darksky_data[1547][1];
    darksky[4] = darksky_data[1961][1];
    darksky[5] = darksky_data[2250][1];

    float lunar[6];
    float lunar_data[7500][2];
    char lunar_sedfile[4096];
    FILE *lunar_fp;

    sprintf( lunar_sedfile, "%s/lunar_sed.txt",dir.c_str());

    lunar_fp = fopen ( lunar_sedfile, "r" );
    for (i = 0; i < 1441; i++){
        fgets( line, 200, lunar_fp );
        sscanf( line, "%f %f\n", &lunar_data[i][0], &temp );
        lunar_data[i][1] = temp * background_brightness / 3.882815E-16;
    }
    fclose( lunar_fp );

    lunar[0] = lunar_data[600][1];
    lunar[1] = lunar_data[1800][1];
    lunar[2] = lunar_data[3200][1];
    lunar[3] = lunar_data[4600][1];
    lunar[4] = lunar_data[5700][1];
    lunar[5] = lunar_data[6700][1];

    float a[6][3] = {{ 11.78, 1.376, -.039 }, { 11.84, 1.411, -.041 }, { 11.84, 1.518, -.057 }, \
                     { 11.40, 1.567, -.064 }, { 10.93, 1.470, -.062 }, { 10.43, 1.420, -.052 }};
    float color[6] = { 0.67, 1.03, 0, -0.74, -1.90, -2.20 };

    float magnitude = 100.0, brightness = 0.0, angle = solarzen / M_PI * 180.0;
    j = filter;
    float alpha = 0.0, beta = 0.0;

    if ( angle <= 106 ) {
        alpha = 1.0 - ( angle - 95.0 ) / 11.0;
        beta = 1.0 - alpha;
        magnitude = a[j][0] + a[j][1] * ( angle - 95.0 ) + a[j][2] * ( angle - 95.0 ) * ( angle - 95.0 );
    }
    else if ( angle >= 254 ){
        alpha = 1.0 - ( 265 - angle ) / 11.0;
        beta = 1.0 - alpha;
        magnitude = a[j][0] + a[j][1] * ( 265.0 - angle ) + a[j][2] * ( 265.0 - angle ) * ( 265.0 - angle );
    }
    else if ((angle > 106) && (angle < 254)) {
        alpha = 0.0;
        beta = 1.0;
        magnitude = a[j][0] + a[j][1] * ( 11.0 ) + a[j][2] * ( 11.0 ) * ( 11.0 );
    }

    brightness = 34.08 * exp( 20.72333 - .92104 * ( magnitude - color[j] ) );

    float lunar_like_magnitude, darksky_like_magnitude;
    float lunar_like_brightness, darksky_like_brightness;
    float lunar_like_photon_count, darksky_like_photon_count;

    lunar_like_brightness = 0.5 * ( brightness - 2.0 * alpha * lunar[j] );
    darksky_like_brightness = 0.5 * ( brightness - 2.0 * beta * darksky[j] );

    if ( lunar_like_brightness < 0 ) lunar_like_brightness = 0.0;
    if ( darksky_like_brightness < 0 ) darksky_like_brightness = 0.0;

    lunar_like_magnitude = 26.33111 - 1.08573 * log( lunar_like_brightness ) + color[j];
    darksky_like_magnitude = 26.33111 - 1.08573 * log( darksky_like_brightness ) + color[j];

    lunar_like_photon_count = expf(-.4 * lunar_like_magnitude * 2.30258509);
    if (lunar_like_photon_count < 0) lunar_like_photon_count=0;

    darksky_like_photon_count = expf(-.4 * darksky_like_magnitude * 2.30258509);
    if (darksky_like_photon_count < 0) darksky_like_photon_count = 0;

    if ((angle > 106) && (angle < 130)) darksky_like_photon_count *= exp( 1 - 24.0 / fabs( angle - 130.0 ) );
    if ((angle > 230) && (angle < 254)) darksky_like_photon_count *= exp( 1 - 24.0 / fabs( angle - 230.0 ) );
    if ((angle >= 130) && (angle <= 230)) darksky_like_photon_count = 0;

    if ((angle > 106) && (angle < 130)) lunar_like_photon_count *= exp( 1 - 24.0 / fabs( angle - 130.0 ) );
    if ((angle > 230) && (angle < 254)) lunar_like_photon_count *= exp( 1 - 24.0 / fabs( angle - 230.0 ) );
    if ((angle >= 130) && (angle <= 230)) lunar_like_photon_count = 0;


    for (diffusetype = 0; diffusetype < 4; diffusetype++){
        if ((diffusetype == 0 && domelight < 100) || (diffusetype == 1 && zenith_v < 100) ||
            (diffusetype == 2 && moonalt > 0) || (diffusetype == 3 && zenith_v < 100)) {

            //POPULATE SOURCES
            ndeci = (long)(180*3600/backRadius);
            over = (long)(60/backRadius);
            for (deci = 0; deci < ndeci; deci += over) {
                dec = ((deci + 0.5 - ndeci/2)/(ndeci))*M_PI;
                nrai = ((long)(ndeci*cos(dec)*2));
                for (rai = 0; rai < nrai; rai += over) {
                    ra = 2*M_PI*((rai + 0.5 - nrai/2)/(nrai));

                    A = cos(dec)*cos(ra - pra);
                    F = focal_length/(sin(pdec)*sin(dec)+A*cos(pdec));
                    yp = F*(cos(pdec)*sin(dec)-A*sin(pdec));
                    xp = F*cos(dec)*sin(ra-pra);
                    xv = (xp * cos(-rotationangle) + yp * sin(-rotationangle));
                    yv = (-xp * sin(-rotationangle) + yp * cos(-rotationangle));
                    currbuffer = backBuffer + (3*backBeta*backRadius + 60)*ARCSEC*focal_length/pixsize;
                    x_max_distance = pixelsx * pixsize/2.0 + currbuffer * pixsize;
                    y_max_distance = pixelsy * pixsize/2.0 + currbuffer * pixsize;
                    dx = xv - centerx - decenterx;
                    dy = yv - centery - decentery;
                    source_dist_x = fabs(cos(chipangle)*dx + sin(chipangle)*dy);
                    source_dist_y = fabs(-sin(chipangle)*dx + cos(chipangle)*dy);

                    dra = fabs(ra-pra);
                    if (dra > M_PI) dra = 2*M_PI-dra;
                    cosdis = sin(dec)*sin(pdec)+cos(dec)*cos(pdec)*cos(dra);
                    if (cosdis>1) cosdis = 1.0;
                    if (cosdis<-1) cosdis = -1.0;
                    dis = acos(cosdis);

                    if ( (source_dist_x <= x_max_distance) && (source_dist_y <= y_max_distance) &&
                         (dis < M_PI/2) ){

                        for (i = 0; i < over; i++) {
                            for (j = 0; j < over; j++) {
                                dec = ((deci + i + 0.5- ndeci/2)/(ndeci))*M_PI;
                                ra = 2*M_PI*((rai + j + 0.5 - nrai/2)/(nrai));

                                A = cos(dec)*cos(ra - pra);
                                F = focal_length/(sin(pdec)*sin(dec) + A*cos(pdec));
                                yp = F*(cos(pdec)*sin(dec) - A*sin(pdec));
                                xp = F*cos(dec)*sin(ra - pra);
                                xv = (xp * cos(-rotationangle) + yp * sin(-rotationangle));
                                yv = (-xp * sin(-rotationangle) + yp * cos(-rotationangle));
                                currbuffer = backBuffer + 3*backBeta*backRadius*ARCSEC*focal_length/pixsize;
                                x_max_distance = pixelsx * pixsize/2.0 + currbuffer * pixsize;
                                y_max_distance = pixelsy * pixsize/2.0 + currbuffer * pixsize;
                                dx = xv - centerx - decenterx;
                                dy = yv - centery - decentery;
                                source_dist_x = fabs(cos(chipangle)*dx + sin(chipangle)*dy);
                                source_dist_y = fabs(-sin(chipangle)*dx + cos(chipangle)*dy);

                                dra = fabs(ra - pra);
                                if (dra > M_PI) dra = 2*M_PI - dra;
                                cosdis = sin(dec)*sin(pdec) + cos(dec)*cos(pdec)*cos(dra);
                                if (cosdis > 1) cosdis = 1.0;
                                if (cosdis < -1) cosdis = -1.0;
                                dis = acos(cosdis);

                                if ( (source_dist_x <= x_max_distance) && (source_dist_y <= y_max_distance) &&
                                     (dis < M_PI/2)){

                                    dra = fabs(ra-moonra);
                                    if (dra > M_PI) dra = 2*M_PI-dra;
                                    cosdis = sin(dec)*sin(moondec) + cos(dec)*cos(moondec)*cos(dra);
                                    if (cosdis > 1) cosdis = 1.0;
                                    if (cosdis < -1) cosdis = -1.0;
                                    angular_sep_radians = acos(cosdis);
                                    angular_sep_degrees  =  angular_sep_radians * 180.0 / M_PI;

                                    scatter_function = 1.08572918*log(2.27E5 * (1.06 + cos(angular_sep_radians) * cos(angular_sep_radians))*
                                                                      pow((0.55/(central_wavelength)), 4.0) +
                                                                      exp((6.15 - angular_sep_degrees / 40.0)*2.30258509));
                                    if ( moonalt < 0 ) moon_magnitude = 10000;
                                    else moon_magnitude = lunar_illuminance - scatter_function;

                                    moon_magnitude = -2.5*log10(lunar_like_photon_count+pow(10.0, -0.4*moon_magnitude));
                                    darksky_magnitude = -2.5*log10(darksky_like_photon_count+pow(10.0, -0.4*zenith_v));

 //zodiacallight_magnitude = 1000.0;
double ZL;
double X;
   double Y;
   double Z;
   double Xec;
   double Yec;
   double Zec;
   double lamda;
   double beta;
   double elongation;
   double inclination;
   // double Brightness;
   double pi = 3.1415926535897;
   double e = 23.5*pi/180;
  double lamda1;
  double beta1;

   X = cos(rai) * cos(deci);
   Y = sin(rai) * cos(deci);
   Z = sin(deci);
   Xec = X;
   Yec = Y*cos(e) + Z*sin(e);;
   Zec = -Y*sin(e) + Z*cos(e);
   lamda = atan(Yec/Xec);
   beta = atan(Zec/sqrt(pow(Xec,2) + pow(Yec,2)));
   lamda1 = lamda * 180/pi;
   beta1 = beta * 180/pi;
   elongation = acos(cos(lamda)*cos(beta));
   if (sin(elongation) == 0){
    inclination =  pi/2;
   }
   if (sin(elongation) != 0){
       inclination = asin(sin(beta)/sin(elongation));
   }


   if (elongation > 10.0*pi/180) {
       ZL = ( 5.6564234022581481E+01) + (9.1801110895424358E+01)*exp((-6.3692890933040702E-02)*lamda1+(6.3752348305553763E-01)) + ( 1.8392739969740077E+02)*exp((-5.3492750304369230E-02)*beta1+(3.4820378322131140E-01)) + (-1.7057210962525505E+02)*pow(exp((-6.3692890933040702E-02)*lamda1+(6.3752348305553763E-01)),2) + (-1.0860879796204463E+02)*pow(exp((-5.3492750304369230E-02)*beta1+(3.4820378322131140E-01)),2) + (6.7664299684838284E+01)*pow(exp((-6.3692890933040702E-02)*lamda1+(6.3752348305553763E-01)),3) + (1.6709516877866797E+01)*pow(exp((-5.3492750304369230E-02)*beta1+(3.4820378322131140E-01)),3) +
           (1.6636986573984145E+03)*exp((-6.3692890933040702E-02)*lamda1+(6.3752348305553763E-01))*exp((-5.3492750304369230E-02)*beta1+(3.4820378322131140E-01)) + (-7.8261380334206297E+02)*pow(exp((-6.3692890933040702E-02)*lamda1+(6.3752348305553763E-01)),2)*exp((-5.3492750304369230E-02)*beta1+(3.4820378322131140E-01)) + (1.8309063327665785E+03)*exp((-6.3692890933040702E-02)*lamda1+(6.3752348305553763E-01))*pow(exp((-5.3492750304369230E-02)*beta1+(3.4820378322131140E-01)),2);
   }
   if (elongation <= 10.0*pi/180) {
       ZL = 4.1*pow(10.0, 6.0);
   }
   zodiacallight_magnitude = -2.5*(log10(ZL)) + 27.78;
 // printf("%e %e\n",ZL,zodiacallight_magnitude);
   zodiacallight_magnitude = 100.0;

                                    // airglow variation
                                    double airglowv;
                                    long ax0, ax1, ay0, ay1;
                                    double dx, dy;
                                    find_linear_wrap(xv, platescale*15.0/3600, airglowScreenSize, &ax0, &ax1, &dx);
                                    find_linear_wrap(yv, platescale*15.0/3600, airglowScreenSize, &ay0, &ay1, &dy);

                                    airglowv = airglowvariation*((double)interpolate_bilinear_float_wrap(airglow, airglowScreenSize,
                                                                                                         ax0, ax1, dx, ay0, ay1, dy));

                                    if (diffusetype == 0) mag = domelight - 2.5*log10(backRadius*backRadius);
                                    if (diffusetype == 1) mag = darksky_magnitude + airglowv - 2.5*log10(backRadius*backRadius);
                                    if (diffusetype == 2) mag = moon_magnitude - 2.5*log10(backRadius*backRadius);
                                    if (diffusetype == 3) mag = zodiacallight_magnitude - 2.5*log10(backRadius*backRadius);
                                    if (mag < 100) {

                                        // printf("%d %e\n",diffusetype,mag+2.5*log10(backRadius*backRadius));

                                        // fprintf(tempfile,"%lf %lf %d %lf\n",ra,dec,diffusetype,mag);

                                        nspatialpar = 0;

                                        sources.id.push_back(0.0);
                                        sources.ra.push_back(ra);
                                        sources.dec.push_back(dec);
                                        if (diffusetype == 0 && domewave == 0.0) sources.sedfilename[nsource] = dir + "/sed_dome.txt";
                                        if (diffusetype == 0 && domewave != 0.0) sources.sedfilename[nsource] = "laser";
                                        if (diffusetype == 1) sources.sedfilename[nsource] = dir + "/darksky_sed.txt";
                                        if (diffusetype == 2) sources.sedfilename[nsource] = dir + "/lunar_sed.txt";
                                        if (diffusetype == 3) sources.sedfilename[nsource] = dir + "/zodiacal_sed.txt";
                                        sources.redshift.push_back(0.0);
                                        sources.gamma1.push_back(0.0);
                                        sources.gamma2.push_back(0.0);
                                        sources.kappa.push_back(0.0);
                                        sources.deltara.push_back(0.0);
                                        sources.deltadec.push_back(0.0);
                                        sources.type[nsource] = diffusetype;
                                        sources.norm[nsource] = pow(10.0, ((mag + 48.6)/(-2.5)));
                                        sources.mag[nsource] = mag;

                                        double normwave = 500.0;
                                        oldsed = 0;
                                        /* read SED file */
                                        if (nsource > 0) {
                                            for (ii = 0; ii < nsource; ii++) {
                                                if (sources.sedfilename[ii] == sources.sedfilename[nsource]) {
                                                    sources.sedptr[nsource] = sources.sedptr[ii];
                                                    oldsed = 1;
                                                    goto skipsedread;
                                                }
                                            }
                                        }

                                        sources.sedptr[nsource] = nsedptr;

                                        if (sedptr == 0) {
                                            nreallocs = 0;
                                            sed_max = 4194304; // 4M elements, 32MB memory
                                            sed_w = static_cast<double*>(malloc((long)(sed_max*sizeof(double))));
                                            sed_c = static_cast<double*>(malloc((long)(sed_max*sizeof(double))));
                                        } else {
                                            if (sedptr > (sed_max - 25000)) {
                                                ++nreallocs;
                                                sed_max = 2*sed_max;
                                                // fprintf(stdout,"%ld reallocs of sed_w and sed_c, now with sedptr=%ld and sed_max=%ld\n",
                                                //         nreallocs, sedptr, sed_max);
                                                sed_w = static_cast<double*>(realloc(sed_w, (long)((sed_max)*sizeof(double))));
                                                sed_c = static_cast<double*>(realloc(sed_c, (long)((sed_max)*sizeof(double))));
                                            }
                                        }

                                        lsedptr = 0;
                                        sprintf(tempstring, "%s", sources.sedfilename[nsource].c_str());
                                        if (flatdir == 1) {
                                            sptr = strtok_r(tempstring, "/", &sptr2);
                                            do {
                                                sptr3 = sptr;
                                                sptr = strtok_r(NULL, "/", &sptr2);
                                            } while (sptr != NULL);
                                            sprintf(tempstring, "%s", sptr3);
                                        }

                                        if (strstr(tempstring, "laser") != NULL) {
                                            normwave = domewave;
                                            closestw = 0;
                                            cdw = 1e30;
                                            badfile = 0;
                                            tempf1 = 0.0;
                                            tempf2 = 0.0;
                                            for (int k = 0; k < 5; k++) {

                                                if (k == 0) {
                                                    tempf1 = 300.0;
                                                    tempf2 = 0.0;
                                                }
                                                if (k == 1) {
                                                    tempf1 = domewave - 1.0;
                                                    tempf2 = 0.0;
                                                }
                                                if (k == 2) {
                                                    tempf1 = domewave;
                                                    tempf2 = 1.98645e-16/(1e-7)/tempf1;
                                                }
                                                if (k == 3) {
                                                    tempf1 = domewave + 1.0;
                                                    tempf2 = 0.0;
                                                }
                                                if (k == 4) {
                                                    tempf1 = 1200.0;
                                                    tempf2 = 0.0;
                                                }

                                                *(sed_w + sedptr + lsedptr) = tempf1;
                                                *(sed_c + sedptr + lsedptr) = tempf2*tempf1*1e-7/(1.98645e-16);

                                                dw = fabs(tempf1 - domewave);

                                                if (dw < cdw) {
                                                    cdw = dw;
                                                    closestw = lsedptr;
                                                }
                                                lsedptr = lsedptr + 1;
                                            }

                                        } else {

                                            if (strstr(tempstring, ".gz") == NULL) {

                                                indafile = fopen(tempstring, "r");
                                                if (indafile == NULL) {
                                                    fprintf(stderr, "Can't find SED file: %s\n", tempstring);
                                                    exit(1);
                                                }
                                                closestw = 0;
                                                cdw = 1e30;
                                                badfile = 0;
                                                while (fgets(line, 4096, indafile)) {
                                                    sscanf(line, "%s %s", tempstring1, tempstring2);
                                                    tempf1 = strtod(tempstring1, NULL);
                                                    tempf2 = strtod(tempstring2, NULL);

                                                    *(sed_w + sedptr + lsedptr) = tempf1;
                                                    *(sed_c + sedptr + lsedptr) = tempf2*tempf1*1e-7/(1.98645e-16);
                                                    if (tempstring2[0] == 'n' || tempstring2[0] == 'N') {
                                                        if (badfile == 0) fprintf(stderr, "Error:   SED file: %s contains a NaN!\n", tempstring);
                                                        *(sed_c + sedptr + lsedptr) = 1e-20;
                                                        badfile = 1;
                                                    }

                                                    dw = fabs(tempf1 - 500.0);

                                                    if (dw < cdw) {
                                                        cdw = dw;
                                                        closestw = lsedptr;
                                                    }
                                                    lsedptr = lsedptr + 1;
                                                    if (lsedptr >= 25000) {
                                                        fprintf(stderr, "Error:  Too many lines in SED file: %s\n", tempstring);
                                                        exit(1);
                                                    }
                                                }
                                                fclose(indafile);

                                            } else {

                                                ingzfile = gzopen(tempstring, "r");
                                                if (ingzfile == NULL) {
                                                    fprintf(stderr, "Can't find SED file: %s\n", tempstring);
                                                    exit(1);
                                                }
                                                closestw = 0;
                                                cdw = 1e30;
                                                badfile = 0;
                                                while (gzgets(ingzfile, line, 4096)) {
                                                    sscanf(line, "%s %s", tempstring1, tempstring2);
                                                    tempf1 = strtod(tempstring1, NULL);
                                                    tempf2 = strtod(tempstring2, NULL);

                                                    *(sed_w + sedptr + lsedptr) = tempf1;
                                                    *(sed_c + sedptr + lsedptr) = tempf2*tempf1*1e-7/(1.98645e-16);
                                                    if (tempstring2[0] == 'n' || tempstring2[0] == 'N') {
                                                        if (badfile == 0) fprintf(stderr, "Error:   SED file: %s contains a NaN!\n", tempstring);
                                                        *(sed_c + sedptr + lsedptr) = 1e-20;
                                                        badfile = 1;
                                                    }

                                                    dw = fabs(tempf1 - 500.0);

                                                    if (dw < cdw) {
                                                        cdw = dw;
                                                        closestw = lsedptr;
                                                    }
                                                    lsedptr = lsedptr + 1;
                                                    if (lsedptr >= 25000) {
                                                        fprintf(stderr, "Error:  Too many lines in SED file: %s\n", tempstring);
                                                        exit(1);
                                                    }
                                                }
                                                gzclose(ingzfile);

                                            }

                                        }

                                        for (jj = 0; jj < lsedptr; jj++) {
                                            if (jj !=  0 && jj != (lsedptr - 1)) {
                                                *(sed_c + sedptr + jj) = *(sed_c + sedptr + jj)*
                                                    (*(sed_w + sedptr + jj + 1) - *(sed_w + sedptr + jj - 1))/2.0;
                                            }
                                            if (jj == 0) {
                                                *(sed_c + sedptr + jj) = *(sed_c + sedptr + jj)*(*(sed_w + sedptr + jj + 1) - *(sed_w + sedptr + jj));
                                            }
                                            if (jj == (lsedptr - 1)) {
                                                *(sed_c + sedptr + jj) = *(sed_c + sedptr + jj)*(*(sed_w + sedptr + jj) - *(sed_w + sedptr + jj - 1));
                                            }
                                        }

                                        tempf1 = 0;
                                        for (jj = 0; jj < lsedptr; jj++) {
                                            tempf1 += *(sed_c + sedptr + jj);
                                        }
                                        for (jj = 0; jj < lsedptr; jj++) {
                                            *(sed_c + sedptr + jj) = *(sed_c + sedptr + jj)/tempf1;
                                        }
                                        if (closestw == 0) {
                                            sed_dwdp[nsedptr] = (*(sed_w + sedptr + closestw + 1) - *(sed_w + sedptr + closestw))/
                                                (*(sed_c + sedptr + closestw))/1.0;
                                        } else {
                                            sed_dwdp[nsedptr] = (*(sed_w + sedptr + closestw + 1) - *(sed_w + sedptr + closestw - 1))/
                                                (*(sed_c + sedptr + closestw))/2.0;
                                        }
                                        if (*(sed_c + sedptr + closestw) <= 0.0) {
                                            printf("Error in SED file; 0 value at 500 nm\n");
                                            sed_dwdp[nsedptr] = 0.0;
                                        }

                                        for (jj = 1; jj < lsedptr; jj++) {
                                            *(sed_c + sedptr + jj) += *(sed_c + sedptr + jj - 1);
                                        }
                                        if (oldsed == 0) {
                                            sed_n[nsedptr] = lsedptr;
                                            sed_ptr[nsedptr] = sedptr;
                                            sedptr = sedptr + lsedptr;
                                            nsedptr++;
                                        }
                                        if (nsedptr >= 10000) {
                                            printf("Error:   Too many SED files\n");
                                            exit(1);
                                        }

                                    skipsedread:;
                                        sources.norm[nsource] = sources.norm[nsource]/(normwave)*(1 + sources.redshift[nsource])*
                                            sed_dwdp[sources.sedptr[nsource]];

                                        sources.spatialname[nsource] = "gauss";
                                        if (sources.spatialname[nsource] == "gauss") {
                                            sources.spatialtype[nsource] = 2;
                                            nspatialpar = 1;
                                        }
                                        sources.spatialpar[nsource][0] = backRadius;
                                        sources.dustnamez[nsource] = "none";
                                        sources.dustname[nsource] = "none";

                                        setup_tangent(pra, pdec, &tpx, &tpy, &tpz);

                                        tangent(sources.ra[nsource] + sources.deltara[nsource], sources.dec[nsource] + sources.deltadec[nsource],
                                                &x, &y, &tpx, &tpy, &tpz);

                                        sources.vx[nsource] = x*cos(rotationangle) - y*sin(rotationangle);
                                        sources.vy[nsource] = x*sin(rotationangle) + y*cos(rotationangle);
                                        sources.vz[nsource] = -1.0;
                                        nn = sqrt((sources.vx[nsource])*(sources.vx[nsource]) +
                                                  (sources.vy[nsource])*(sources.vy[nsource]) + 1);
                                        sources.vx[nsource] = sources.vx[nsource]/nn;
                                        sources.vy[nsource] = sources.vy[nsource]/nn;
                                        sources.vz[nsource] = sources.vz[nsource]/nn;
                                        nsource++;

                                    }

                                }


                            }
                        }
                    }

                }
            }
        }
    }

    return(0);
}


int Observation::filterTruncateSources () {

    double filterlow,  filterhigh;
    long lowptr, highptr;
    double lowvalue, highvalue;

    filterlow = 0;
    filterhigh = 12000;

    for (long i = 0; i < nsedptr; i++) {
        highvalue = 0;
        lowvalue = 0;
        lowptr = 0;
        highptr = 0;
        for (long j = 0; j < sed_n[i]; j++) {

            if (*(sed_w + sed_ptr[i] + j) < filterlow) {
                lowptr = j;
                lowvalue = *(sed_c + sed_ptr[i] + j);
            }
            if (*(sed_w + sed_ptr[i] + j) < filterhigh) {
                highptr = j;
                highvalue = *(sed_c + sed_ptr[i] + j);
            }

        }

        sed_corr[i]=(*(sed_c + sed_ptr[i] + highptr) - *(sed_c + sed_ptr[i] + lowptr));
        for (long j = lowptr; j < highptr + 1; j++) {
            *(sed_c + sed_ptr[i] + j)=(*(sed_c + sed_ptr[i] + j) - lowvalue)/
                (highvalue - lowvalue);
        }
        sed_ptr[i] = sed_ptr[i] + lowptr;
        sed_n[i] = highptr - lowptr;
    }

    for (long i = 0; i < nsource; i++) {
        sources.norm[i] = sources.norm[i]*sed_corr[sources.sedptr[i]];
    }

    return(0);

}
