///
/// @package phosim
/// @file image.h
/// @brief header for image class
///
/// @brief Created by:
/// @author John R. Peterson (Purdue)
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///

#include <fitsio.h>
#include <fitsio2.h>
#include <fftw3.h>
#include <vector>

#include "galaxy.h"
#include "dust.h"
#include "observation.h"
#include "raytrace.h"
#include "surface.h"
#include "coating.h"
#include "silicon.h"
#include "perturbation.h"
#include "screen.h"
#include "air.h"
#include "medium.h"
#include "obstruction.h"
#include "chip.h"
#include "contamination.h"
#include "photon.h"

class Image : public Observation {

 public:

    // objects
    Galaxy galaxy;
    Dust dust;
    Surface surface;
    Coating coating;
    Silicon silicon;
    Air air;
    Perturbation perturbation;
    Screen screen;
    Medium medium;
    Obstruction obstruction;
    Contamination contamination;
    Chip chip;

    // global optimization data
    double *opd;
    double *opdcount;
    double *dynamicTransmission;
    int *satupmap;
    int *satdownmap;

    // photon
    Photon photon;
    // photon specific-data (will turn into structure later)
    double saveRand[MAX_BOUNCE];

    // setup and loop
    int atmSetup();
    int telSetup();
    int photonLoop();
    int dynamicTransmissionOptimization(long k, long *lastSurface, long *preGhost, long waveSurfaceIndex, long straylightcurrent);

    // physics
    int getWavelengthTime(double *wavelength, double *time, long source);
    int domeSeeing(Vector *angle);
    int tracking(Vector *angle, double time);
    int atmosphericDispersion(Vector *angle);
    int largeAngleScattering(Vector *largeAngle);
    int secondKick(Vector *largeAngle);
    int diffraction(Vector *position, Vector angle, Vector *largeAngle);
    int samplePupil(Vector *position, long long ray);
    int transmissionCheck(double transmission, long surfaceIndex, long waveSurfaceIndex);
    int transmissionPreCheck(long surfaceIndex, long waveSurfaceIndex);
    int chooseSurface(long *newSurface, long *oldSurface);
    int findSurface(Vector angle, Vector position, double *distance, long surfaceIndex);
    int goldenBisectSurface(double a, double b, double c, double *z, Vector angle,
                            Vector position, double *distance, long surfaceIndex);
    int getIntercept(double x, double y, double *z, long surfaceIndex);
    int getDeltaIntercept(double x, double y, double *zv, long surfaceIndex);
    int bloom(int saturatedFlag);
    int photonSiliconPropagate(Vector *angle, Vector *position, double lambda, Vector normal,
                               double dh, long waveSurfaceIndex);
    int electronSiliconPropagate(Vector *angle, Vector *position);
    int contaminationSurfaceCheck(Vector position, Vector *angle, long surfaceIndex);
    double airIndexRefraction();
    double surfaceCoating(double wavelength, Vector angle,
                          Vector normal, long surfaceIndex, double *reflection);
    double cloudOpacity(long layer);
    double atmosphereOpacity(Vector angle, long layer);
    double fringing (Vector angle, Vector normal, double wavelength, double nSi, double thickness);
    void getAngle(Vector *angle, double time, long source);
    void getDeltaAngle(Vector *angle, Vector *position, long source);
    void newRefractionIndex(long surfaceIndex);
    void atmospherePropagate(Vector *position, Vector angle, long layer, int mode);
    void atmosphereIntercept(Vector *position, long layer);
    void atmosphereRefraction(Vector *angle, long layer, int mode);
    void atmosphereDiffraction(Vector *angle);
    void transform(Vector *angle, Vector *position, long surfaceIndex);
    void transformInverse(Vector *angle, Vector *position, long surfaceIndex);
    void interceptDerivatives(Vector *normal, Vector position, long surfaceIndex);
    void cosmicRays(long long *raynumber);
    void saturate(long source, Vector *largeAngle);

    // output
    void writeImageFile();
    void writeOPD();
    void readCheckpoint(int checkpointcount);
    void writeCheckpoint(int checkpointcount);
    void cleanup();

};
