///
/// @package phosim
/// @file instrumentfiles.h
/// @brief Class to make instrument files for program insturment
///
/// @brief Created by:
/// @author Glenn Sembroski
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///

#ifndef INSTRUMENTFILES_H
#define INSTRUMENTFILES_H

#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <map>
#include <stdexcept>
#include <string>
#include <vector>
#include <sstream>

#include "../ancillary/readtext.h"
#include "../raytrace/basic_types.h"
#include "../raytrace/rng_mwc.h"
#include "../raytrace/parameters.h"
using namespace RandomNumbers;

using readtext::readText;

class InstrumentFiles {
 public:
    InstrumentFiles();
    ~InstrumentFiles();

    void makeTrackingFile(std::string trackingFileName, double vistime, 
			  double jittertime);
    void readActuatorFile(std::string actuatorFileName, 
			readText& pars, readText& controlPars,
                        std::vector < std::vector < double> >& actuatorMatrix,
			std::vector < double >& actuatorDistance,
			  std::vector < double >& actuatorError);
    void writeBodyFile(std::map< int, std::vector< double>* >& body, 
		       std::string opticsFileName, std::string obsID);
    void readoutPars(readText& focalPlaneLayoutPars,readText& segmentationPars,
		     std::string readoutString, int camConfig);

    void focalPlanePars(readText& focalPlaneLayoutPars,
                        std::string outChipString, int camConfig);


    void makeSurfaceMap(std::string opticsFile);

 private:
    std::map< std::string, int > fSurfaceMap;
    std::map< std::string, int >::iterator fSurfaceMapPos;
    int fLastDevice;
    int fLastSurface;
    bool haveBody;
};
#endif
