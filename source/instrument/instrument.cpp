///
/// @package phosim
/// @file instrument.cpp
/// @brief instrument  application.
///
/// @brief Created by
/// @author Nathan Todd (Purdue)
///
/// @brief Modified by
/// @author John R. Peterson (Purdue)
/// @author En-Hsin Peng (Purdue)
/// @author Glenn Sembroski (Purdue)
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///

#include "instrument.h"

// ***********************************************************************
// This program reads in the various insturment definition files and creates 
// the necessary control files for the various PhoSim system programs.
// ***********************************************************************

int getModeFromTag(std::string tag) {
    // Find the index for this tag
    int num = -1;
    //The allowed tags.
    if      (tag=="phi")   num = 1;
    else if (tag=="psi")   num = 2;
    else if (tag=="theta") num = 3;
    else if (tag=="xdis")  num = 4;
    else if (tag=="ydis")  num = 5;
    else if (tag=="zdis")  num = 6;
    else if (tag=="z1")    num = 7;
    else if (tag=="z2")    num = 8;
    else if (tag=="z3")    num = 9;
    else if (tag=="z4")    num = 10;
    else if (tag=="z5")    num = 11;
    else if (tag=="z6")    num = 12;
    else if (tag=="z7")    num = 13;
    else if (tag=="z8")    num = 14;
    else if (tag=="z9")    num = 15;
    else if (tag=="z10")   num = 16;
    else if (tag=="z11")   num = 17;
    else if (tag=="z12")   num = 18;
    else if (tag=="z13")   num = 19;
    else if (tag=="z14")   num = 20;
    else if (tag=="z15")   num = 21;
    else if (tag=="z16")   num = 22;
    else if (tag=="z17")   num = 23;
    else if (tag=="z18")   num = 24;
    else if (tag=="z19")   num = 25;
    else if (tag=="z20")   num = 26;
    else if (tag=="z21")   num = 27;
    // Note that we need to have gTotalNumTags equal to the
    // highest value num can take here. (Set in instrument.h)
    // If you add more tags, be sure to increase gTotalNumTags
    return num;
}
// ********************************************************************

void updateControl(readText& controlPars, readText& pars)
// ********************************************************************
// updates controlPars readText command list with values from input command 
// file (which is pars).
// May add and/or replace existing entries in controlPars.
// ********************************************************************
{
    // First we need to search through all the ObsExtra list of commands for 
    // the the control type commands. These commands will have as the first 
    // key a surace designator. The second key will match a "mode"( see 
    // getModeFromTag above). We know we have a control command when we find
    // a valid mode as the second key.
    // Note we have to ignore  "dlsm" commands
    int numParCommands = pars.getSize();
    std::string lineValue;

    for (int i = 0;i<numParCommands;i++){
        std::string line(pars[i]); //Get the next line from the obsExtra file.
        //Ignore actuator and dlsm commands
        if (readText::getKey(line, "dlsm", lineValue)){
            continue;
        }
        else {
            std::istringstream iss(line);
            std::string key;
            std::string tag;
            iss>> key>>tag;
            // ***********************
            // Test that this is a control mode tag. This is how we identify a 
            // control command. If it is a control command, append the command to 
            // the control file and count how many
            // ***********************
            if(getModeFromTag(tag)>0){
                controlPars.add(line); 
            }
            continue;
        }
    }
    // ***********************************************************************
    // NOTE: THE FOLLOWING ALGORITUM RETIANS THE ORDER OF THE SURFACES AS 
    // PRESENTED IN THE INITIAL CONTROL.TXT FILE. THIS IS NECESSARY SINCE THE 
    // ACTUATOR.TXT FILE LINES ARE IN THE SAME ORDER!
    // ***********************************************************************
    // Remove duplicate key-mode commands with the ones at the end of the
    // control.txt file having priority. (So go backwards through the 
    // controlPars command list
    // *************************************
    int numControlCommands = controlPars.getSize();
    for(int i = numControlCommands-1;i>0;i--) {
        //Get key and mode
        std::string line1(controlPars[i]);
        std::istringstream iss1(line1);
        std::string key1;
        std::string tag1;
        iss1>> key1 >> tag1;
    
        // Go backwards through the rest of this file looking for a previous 
        // instance of the key-mode pair.
        // Not most efficent but it is clear. Well maybe not. What we are doing
        // is taking the last instace of a key.tag pair and replacing the next to 
        // last instance with it.  If there is an even earlier instance it
        // will eventually be replaced also by this line when we hit this
        // replaced one. Thats clear right?

        for(int j = i-1;j>=0;j--) {
            //Get key and mode
            std::string line2(controlPars[j]);
            std::istringstream iss2(line2);
            std::string key2;
            std::string tag2;
            iss2>> key2 >> tag2;
            if ( key1 == key2 && tag1 == tag2 ) {
                // Duplicate, Replace this line with the later one.
                controlPars.setLine(j, line1);
                //Remove latest line from controlPars
                controlPars.eraseLine(i);//This works becuase we never look after 
                //this index (i) again in this method.
                continue;  
            }
        }
    }
    // At this point there are no key-mode duplicates in controlPars and the 
    // later commands have taken precidence over erlier ones with same key-mode
    return;
}
// **************************************************************************

void createBody(std::map< int, std::vector<double>*>&  bodyMap,
                readText& controlPars, readText& pars,
                std::vector < std::vector < double> >& actuatorMatrix,
                std::vector < double >& actuatorDistance,
                std::vector < double >& actuatorError,
                long  obsseed)
// ***************************************************************************
// Fill the body vectors using the commands in the controlPars and
// the actuator arrays.
// ***************************************************************************
{
    // Use a map here for the body data to allow for any number of different 
    // devices in any order appearing in the control.txt file. We can add them
    // as they show up
    // Note we are using pointers to vectors on the heap for the data in the 
    // body map.

    //Init the random number generator(RNG) (not sure we need this or even
    //  want it since we also initalize RNG in main, this may be dupilcation!)
    int seed=obsseed;
    if (obsseed == -1){
        RngSetSeedFromTime();
    }
    else{
        RngSetSeed32(seed);
    }
    RngUnwind(10000);
    RngSetSeed32_reseed(1000);

    // Get all the values we will need to determine the body values
    double scaleOne = 1.0;
    double scaleTwo = 1;
    double seeing = 0.0;
    double domeseeing = 0.1;
    double zenithDeg = 0.0;
    double zenithRad = 0.0;
    double temperature0 = 20.0;
    double temperatureC = 20.0;
    double temperatureVarC = 0.0;
    double pressure0 = 520.0;
    double pressuremmHg = 520.0;
    double pressureVarmmHg = 0.0;
    double altitudeDeg = 90.0;
    double altitudeVarDeg = 0.0;

    // Go through the obsExtra file (pars) and get most up-to-date values.
    int numPars = pars.getSize();
    for (size_t t(0); (int) t < numPars; t++) {
        std::string line(pars[t]); //Get the next line from the obsExtra file.
        readText::get(line, "scalefactorone", scaleOne);
        readText::get(line, "scalefactortwo", scaleTwo);
        readText::get(line, "constrainseeing", seeing);
        readText::get(line, "domeseeing", domeseeing);
        if (readText::getKey(line, "zenith", zenithDeg)) {
            zenithRad = zenithDeg*kPi/180.;
        }
        readText::get(line, "temperature", temperatureC);
        readText::get(line, "tempvar", temperatureVarC);
        readText::get(line, "pressure", pressuremmHg);
        readText::get(line, "pressvar", pressureVarmmHg);
        if (readText::getKey(line, "altitude", altitudeDeg)) {
            zenithRad = (90.-altitudeDeg)*kPi/180.;
        }
        readText::get(line, "altvar", altitudeVarDeg);
    }

    // ******************************************************************
    // Iterate through the control lines(All duplicates have been removed and 
    // updated. All comments  and empty lines have been removed. Order of
    // these lines is retained form the original control.txt file which must
    // match the actuator.txt surface order.
    // ******************************************************************
    int numKeys = controlPars.getSize();


    // ************************************************
    //Debug: to match ran number seq from original createBody
    // Make a map (which is sorted
    // ************************************************
    //std::map< std::string, std::string>  controlMap; 
    //for (int keyNum = 0; keyNum < numKeys; keyNum++){
    //  std::string line1(controlPars[keyNum]); //Get the next line from the 
    // control lines.        
    //  std::istringstream iss1(line1);
    //  std::string deviceStr;
    //  std::string tag1;
    //  iss1>> deviceStr >> tag1;
    //  std::string key = deviceStr + " " + tag1;
    //  controlMap[key]=line1;
    //}

   
    //std::map< std::string, std::string>::iterator  controlPos;
    //int keyNum=-1;
    //for (controlPos=controlMap.begin(); controlPos!= controlMap.end(); 
    //                                                        ++controlPos){
    //std::string line1=controlPos->second;
    //keyNum++;
    // ****************eND dEBUG**************************

    for (int keyNum = 0; keyNum < numKeys; keyNum++){
        std::string line1(controlPars[keyNum]); //Get the next line from the 
        // control lines.        
        std::istringstream iss1(line1);
        std::string deviceStr;
        std::string tag1;
        iss1>> deviceStr >> tag1;
	
        int mode = getModeFromTag(tag1);
        if(mode == -1){
            std::cout << "Illegal mode in control.txt or command file: " 
                      << deviceStr << ":" << tag1 << std::endl;
            continue;
        }

        // ******************************************************************
        // Ok, we can now fill in an optics vector though we will throw most 
        // of it away. gDevCol is a global variable set in instrument.h
        // ******************************************************************
        std::vector< double > optics;
        optics.resize(gDevCol,0);
      
        //Start filling
        optics.at(0) = mode;
        double bodyValue;
        for(int i = 1; i < gDevCol; i++){
            iss1 >> bodyValue;
            optics.at(i) = bodyValue;
        }

        //Above got us the "mandatory" stuff, now get any possible extra stuff
        // Note that the next values are first the device id for this line
        // followed by the device id's that are "connected" to this device
        // (ex 2 sides of a lens)
        while(iss1>>bodyValue){
            optics.push_back(bodyValue);
        }


        // *****************************************************************
        // Now we get into the filling of the body array
        // *****************************************************************

        double bodyQuantity = 0.0;

        // Fill the body as needed
        // I don't know what mostof this stuff is
        if ((int)optics.at(1)==1){
            bodyQuantity = optics.at(2)*random_gaussian()+optics.at(3);
        }
        else if((int)optics.at(1)==2){
            bodyQuantity = (optics.at(3)-optics.at(2))*RngDouble()+optics.at(2);
        }

        if ((int)optics.at(4)==1){
            bodyQuantity *= scaleOne*sqrt(seeing*seeing+domeseeing*domeseeing)/
                0.67*pow(1/cos(zenithRad),3./5.);
        }
        else if ((int)optics.at(4) == 2) {
            bodyQuantity *= scaleTwo;
        }
        //fabrication error
        bodyQuantity +=optics.at(5) * RngDouble_reseed();

        bodyQuantity +=                                         //thermal
            optics.at(6) * (temperatureC - temperature0 + temperatureVarC * random_gaussian());

        bodyQuantity +=                                        //pressure
            optics.at(7) * (pressuremmHg - pressure0 + pressureVarmmHg * random_gaussian());

        bodyQuantity += optics.at(8) * (altitudeDeg + altitudeVarDeg * random_gaussian());
      
        bodyQuantity += optics.at(9) * random_gaussian();    //hidden variable

        // Now apply the actuator stuff
        int numActuator = actuatorError.size();
        for (int i = 0;i<numActuator;i++){
            bodyQuantity+=
                actuatorMatrix.at(keyNum).at(i)*(actuatorDistance.at(i)+
                                                 actuatorError.at(i)*random_gaussian());
        }


        int modeIndex = (int)(optics.at(0)-1);   //for things like xdis z12 phi

        int devIndex  = (int)optics.at(gDevCol); // for things like M1 M13 
        // camera etc


        // See if we already have this device in our pBodyMap, if not make
        // a new one
        std::vector < double >*  pBodyDevice;
        std::map< int, std::vector< double >* >::iterator bodyPos =
            bodyMap.find(devIndex);
        if(bodyPos==bodyMap.end()){
            // Need to make a new body vector on the heap for this device and 
            // get a pointer to it
            // I'm worried about the hardwiring of 28 here.
            pBodyDevice = new std::vector< double >;
            pBodyDevice->resize(gTotalNumTags,-5.0);
            bodyMap[devIndex] = pBodyDevice;
        }
        else{
            pBodyDevice = bodyPos->second;
        }

        pBodyDevice->at(modeIndex) = bodyQuantity;

        //Once body array is done see if we this is a zernike type command
        // and this device is "attached" to others (so that they have the same
        // body commands. See above for comments on how this works.
        int numOptics = optics.size();
        if(numOptics-1>=gDevCol+1){
            for(int k = gDevCol+1;k<numOptics;k++) {
                int zDevIndex  = (int)optics.at(k);


                std::map< int, std::vector< double >* >::iterator zBodyPos =
                    bodyMap.find(zDevIndex);
                if(zBodyPos==bodyMap.end()){
                    pBodyDevice = new std::vector< double >;
                    pBodyDevice->resize(gTotalNumTags,-5.0);
                    bodyMap[zDevIndex] = pBodyDevice;
                }
                else{
                    pBodyDevice = zBodyPos->second;
                }
                pBodyDevice->at(modeIndex) = bodyQuantity;
            }
        }

    }
    return;
}
// **********************************************************************

int main(void) {
    // ***********************************************************************
    // This program reads in the various insturment definition files and creates 
    // the necessary control files for the various PhoSim sytem programs.
    // ***********************************************************************
    // It starts with the file obsExtra_"observationID"_.pars as input through cin
    // The obsExtra(assigned to cin) file is the sum of  the 
    // obs_"observationID"_.pars file (subset of the the default instance followed
    // by the instance file (without any  "objects") and as far as I can tell 
    // only the "dlsm" commands from the command line. (maybe more, not sure) 
    // ***********************************************************************
    std::cout<<"-------------------------------------------------------------"
        "-----------------------------"<<std::endl;
    std::cout<<"Instrument Configuration"<<std::endl;
    std::cout<<"-------------------------------------------------------------"
        "-----------------------------"<<std::endl;

    // Read parameters from stdin. cin has been fed the obsExtra file which 
    // has some of the instance file and all of the command line -c command 
    // file
    readText pars(std::cin);
    int parsNumberObsExtra = pars.getSize();

    // Set some default values/flags.
    std::string instrumentDir = "../data/lsst";
    std::string directory = ".";
    std::string obsID = "NO-obsid";
    double vistime = 15.0;
    long obsseed = -1;         //Default seed (something unique to this run)
    int filter = 0;            //Default filter;cause optics_0.txt to be used
    double altitude = 90.0 * M_PI/180.;  //Pointing starts verticle
    double zenith = M_PI/2.0 - altitude;
    double temperature = 20.0;
    double jittertime = 0.1;
    std::vector<std::string> pertSurf;    //For dlsm (in development)
    int camConfig;
    // *********************************************************************
    // Go through the obsExtra file line by line. If there are any duplicate 
    // commands, the values in the ones at the end will override previous 
    // values from previous commands. ie.
    // Values from the "commands" file (from the -c option on the phosim
    // command line placed at the end of obsExtra) will override those from 
    // the instance file (which was placed at the start of obsExtra)
    // **********************************************************************
    for (size_t t(0); (int) t < parsNumberObsExtra; t++) {
        std::string line(pars[t]); //Get the next line from the obsExtra file.
        // Check for various commands whose values we need.
        readText::get(line, "obshistid", obsID);   //Run (observation) ID.
        readText::get(line, "instrdir", instrumentDir); //telescope directory
        readText::get(line, "outputdir",  directory);     //Output directory
        readText::get(line, "obsseed", obsseed);   //Random number seed
        readText::get(line, "filter", filter);     //filter value (sets which 
        //optic*.txt file is used
        readText::get(line, "temperature", temperature);  //Observatory temp.
        readText::get(line, "vistime",  vistime);  //Length of run in seconds.
        readText::get(line, "jittertime",  jittertime);
        readText::get(line, "camconfig",   camConfig);
        if (readText::getKey(line, "altitude", altitude)) {
            altitude *= M_PI/180.;          //Telescope pointing altitude in Deg
            zenith = M_PI/2.0 - altitude;
        }
        if (readText::getKey(line, "zenith", zenith)) {
            zenith *= M_PI/180.0;         //Or can use zenith in deg. This takes
        }                               //precedence
        std::istringstream iss(line);
        std::string keyName;
        iss >>keyName;
        if (keyName == "dlsm" ) {       //experimental. In development
            size_t surfaceIndex;
            iss >> surfaceIndex;
            std::string linkSurfaces;
            std::getline(iss, linkSurfaces);
            if(surfaceIndex>=pertSurf.size()) pertSurf.resize(surfaceIndex+1);
            pertSurf[surfaceIndex].assign(linkSurfaces);
        }

    }

    // Read optics_0.txt file to get map of surface names to surface ID
    // This thus requires that for every instrument(telescope) we simulate 
    // that there is an optics_0.txt file in the intrument directory
    std::string opticsFile = instrumentDir + "/optics_0.txt";
    instrumentFiles.makeSurfaceMap(opticsFile);

    //Init the random number generator
    if (obsseed == -1) {
        RngSetSeedFromTime();
    } else {
        RngSetSeed32(obsseed);
    }
    RngUnwind(10000);


    //Create and write the "tracking" file.
    std::string trackingFileName = directory + "/tracking_" + obsID + ".pars";
    instrumentFiles.makeTrackingFile(trackingFileName, vistime, jittertime);

    std::string controlFileName = instrumentDir + "/control.txt";
    readText controlPars(controlFileName);


    // Update the control map with any control commands that were found in the
    // input command file.
    // These values will over ride any existing or add to the body commands in
    // control
    updateControl(controlPars,pars);

    // Read  in the actuator file: We need the actuator values so we can fill 
    // in the body commands.
    std::vector < std::vector < double> > actuatorMatrix;
    std::vector < double >  actuatorDistance;
    std::vector < double >  actuatorError;
    
    std::string actuatorFileName = instrumentDir + "/actuator.txt";
    instrumentFiles.readActuatorFile( actuatorFileName, pars, controlPars, 
                                      actuatorMatrix,actuatorDistance, 
                                      actuatorError);

    // Define the body matrix map.
    // Remember to always refer to bodyMap by reference in subroutine calls for
    // efficency/performance
    std::map< int, std::vector<double>*>  bodyMap;

    // Now go through our  our controlPars and fill the body array
    createBody(bodyMap,controlPars, pars, actuatorMatrix, actuatorDistance,actuatorError,obsseed);

    std::string opticsFileName = directory + "/optics_" + obsID + ".pars";
    instrumentFiles.writeBodyFile(bodyMap, opticsFileName, obsID);

    std::string focalPlaneLayoutFileName = instrumentDir + 
        "/focalplanelayout.txt";
    readText focalPlaneLayoutPars(focalPlaneLayoutFileName);

    std::string segmentationFileName = instrumentDir + "/segmentation.txt";
    readText segmentationPars(segmentationFileName);

    std::string readoutString = directory + "/readout_" + obsID +"_";
    instrumentFiles.readoutPars(focalPlaneLayoutPars, segmentationPars, 
                                readoutString, camConfig);



    //body, chipangle, izernike, qevariation
    std::string outChipString = directory +"/chip_" + obsID + "_";
    instrumentFiles.focalPlanePars(focalPlaneLayoutPars, outChipString,
                                   camConfig);

    //dlsm
    for (size_t t(0); t<pertSurf.size(); t++){
        if (pertSurf[t] != "") {
            std::cout<<"surface # "<< t<<" link:"<<pertSurf[t]<<std::endl;
            dlsm((int) t, pertSurf[t], obsseed, obsID, filter, instrumentDir, 
                 zenith, temperature);
        }
    }
    return 0;
}
// ***********************************************************************

